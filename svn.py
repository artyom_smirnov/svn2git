import os
import re

from any2git import log, run_and_wait, log_exit

def upstream_prefix():
    return 'svnupstream'


def init_mirror(profile):
    os.chdir(profile.mirror_base)
    log("Clone mirror from %s to %s" % (profile.upstream, profile.mirror))
    args = ['git', 'svn', 'clone', '--stdlayout', '--prefix', '%s/' % upstream_prefix()]
    if profile.authors_file:
        args.append('--authors-file=%s' % profile.authors_file)
    if profile.authors_prog:
        args.append('--authors-prog=%s' % profile.authors_prog)
    args += [profile.upstream, profile.mirror_dir]
    (rc, out, err) = run_and_wait(args)
    if rc:
        log_exit(err, send_email=False)


def fetch_commits(p, commits):
    log("Fetching new commits from upstream %s" % p.upstream)
    args = ['git', 'svn', 'fetch']
    if p.authors_file:
        args.append('--authors-file=%s' % p.authors_file)
    if p.authors_prog:
        args.append('--authors-prog=%s' % p.authors_prog)
    (rc, out, err) = run_and_wait(args)
    if rc:
        log_exit(err)

    r = re.compile(r'r([0-9]+) = ([a-f0-9]+) \(refs/remotes/(.*)\)')
    for l in out.split('\n'):
        res = r.match(l)
        if res:
            hash = res.group(2)
            branch = res.group(3)
            (rc, out, err) = run_and_wait(['git', 'log', '--format=%B', '%s^..%s' % (hash, hash)])
            commits.append((branch, hash, out))
