try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

import sys


class Configuration:
    log_string = StringIO()
    log_file = sys.stdout
    profile_name = 'UNKNOWN'
    mirror_dir = 'UNKNOWN'
    email_server = 'localhost'
    email_from = None
    email_to_always = []
    email_to_onfail = []
    init_mirror = None
    fetch_commits = None
    upstream_prefix = None
