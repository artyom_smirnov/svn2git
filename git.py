import os
from any2git import log, run_and_wait, log_exit


def upstream_prefix():
    return 'origin'


def init_mirror(profile):
    os.chdir(profile.mirror_base)
    log("Clone mirror from %s to %s" % (profile.upstream, profile.mirror))
    (rc, out, err) = run_and_wait(['git', 'clone', profile.upstream, profile.mirror_dir])
    if rc:
        log_exit(err, send_email=False)


def fetch_commits(p, commits):
    old_hash = {}

    for upstream_branch, _ in p.branch_mapping.items():

        (rc, out, err) = run_and_wait(['git', 'rev-parse', 'remotes/%s/%s' % (upstream_prefix(), upstream_branch)])
        if rc:
            log_exit(err)

        old_hash[upstream_branch] = out.strip()

    log("Fetching new commits from upstream %s" % p.upstream)
    (rc, out, err) = run_and_wait(['git', 'fetch', upstream_prefix()])
    if rc:
        log_exit(err)

    for upstream_branch, _ in p.branch_mapping.items():
        (rc, out, err) = run_and_wait(['git', 'rev-parse', 'remotes/%s/%s' % (upstream_prefix(), upstream_branch)])
        if rc:
            log_exit(err)
        new_hash = out.strip()

        (rc, out, err) = run_and_wait(['git', 'rev-list', 'remotes/%s/%s' % (upstream_prefix(), upstream_branch),
                                       '%s..%s' % (old_hash[upstream_branch], new_hash)])
        if rc:
            log_exit(err)

        for hash in out.split():
            (_, msg, _) = run_and_wait(['git', 'log', '--format=%B', '%s^..%s' % (hash, hash)])
            commits.append(('%s/%s' % (upstream_prefix(), upstream_branch), hash, msg))
