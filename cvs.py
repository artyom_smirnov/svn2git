import os
import re

from any2git import log, run_and_wait, log_exit


def upstream_prefix():
    return 'cvsupstream'


def init_mirror(profile):
    os.mkdir(profile.mirror)
    os.chdir(profile.mirror)
    log("Clone mirror from %s to %s" % (profile.upstream, profile.mirror))
    (rc, out, err) = run_and_wait(['git', 'cvsimport', '-a', '-p', '-x', '-v', '-r', upstream_prefix(), '-d',
                                   profile.upstream, profile.upstream_module])
    if rc:
        log_exit(err, send_email=False)


def fetch_commits(p, commits):
    log("Fetching new commits from upstream %s" % p.upstream)
    (rc, out, err) = run_and_wait(['git', 'checkout', 'master'])
    if rc:
        log_exit(err)

    (rc, out, err) = run_and_wait(['git', 'cvsimport', '-a', '-p', '-x', '-v', '-r', upstream_prefix(), '-d',
                                   p.upstream, p.upstream_module])
    if rc:
        log_exit(err)

    r1 = re.compile(r'Committed patch .* \((.*) [0-9]* .*\)')
    r2 = re.compile(r'Commit ID ([a-fA-F0-9]*)')

    match = 0
    branch = None
    hash = None
    for l in out.split('\n'):
        if match == 0:
            res = r1.match(l)
            if res:
                branch = res.group(1)
                match = 1
        else:
            res = r2.match(l)
            if res:
                hash = res.group(1)
                (rc, out, err) = run_and_wait(['git', 'log', '--format=%B', '%s^..%s' % (hash, hash)])
                commits.append(('%s/%s' % (upstream_prefix(), branch), hash, out))
                match = 0
