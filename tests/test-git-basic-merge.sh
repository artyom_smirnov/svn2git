CREATE_TEST_GIT=1

function run_test()
{
    cat<<EOL > config
profiles:
    git_test:
        repo:
            type: git
            upstream: $TEST_GIT
            mirror: $ROOT/mirror.git
            downstream: $ROOT/downstream.git

        branch_mapping:
            master: master

        merging:
            strategy: merge
EOL
    $ANY2GIT -c config init | date_filter > /dev/null

    git clone downstream.git downstreamwc.git
    cd downstreamwc.git
    git log --topo-order --oneline |cut -c 9-
    cd ..

    cd $TEST_GIT_WC
    touch test2
    git add test2
    git commit -m "Commit 2" |& hash_filter
    git push |& hash_filter
    cd ..

    $ANY2GIT -c config sync | date_filter |& hash_filter > /dev/null

    cd downstreamwc.git
    git pull |& hash_filter
    git log --topo-order --oneline |cut -c 9-
    cd ..
}

function get_stdout()
{
    cat<<EOL
(HEAD -> master, origin/master, origin/HEAD) Initial commit
[master HASH] Commit 2
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 test2
To /tmp/any2gittestroot/test.git
   [HASH]..[HASH]  master -> master
From /tmp/any2gittestroot/downstream
   [HASH]..[HASH]  master     -> origin/master
Updating [HASH]..[HASH]
Fast-forward
 test2 | 0
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 test2
(HEAD -> master, origin/master, origin/HEAD) Commit 2
Initial commit
EOL
}

function get_stderr()
{
    cat<<EOL
Cloning into 'downstreamwc.git'...
done.
EOL
}
