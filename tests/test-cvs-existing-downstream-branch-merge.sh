CREATE_TEST_CVS=1

function run_test()
{
    cat<<EOL > config
profiles:
    cvs_test:
        repo:
            type: cvs
            upstream: $TEST_CVS
            upstream_module: $TEST_CVS_MODULE
            mirror: $ROOT/mirror.gitcvs
            downstream: $ROOT/downstream.git

        branch_mapping:
            master: master

        merging:
            strategy: merge
EOL
    mkdir downstream.git
    cd downstream.git
    git init --bare
    cd ..

    git clone downstream.git downstreamwc.git
    cd downstreamwc.git
    touch myfile
    git add myfile
    git commit -m 'Commit in downstream' | hash_filter
    git push
    cd ..

    $ANY2GIT -c config init --existing-downstream | date_filter > /dev/null
    $ANY2GIT -c config sync | date_filter > /dev/null

    cd $TEST_CVS_WC
    touch test2
    cvs add test2
    cvs commit -m "Commit 2"
    cd ..

    $ANY2GIT -c config sync | date_filter | hash_filter > /dev/null

    cd $ROOT/downstreamwc.git
    git pull |& hash_filter
    git log --topo-order --oneline |cut -c 9-
    cd ..
}

function get_stdout()
{
    cat<<EOL
Initialized empty Git repository in /tmp/any2gittestroot/downstream.git/
[master (root-commit) HASH] Commit in downstream
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 myfile
/tmp/any2gittestroot/test.cvs/test_module/test2,v  <--  test2
initial revision: 1.1
From /tmp/any2gittestroot/downstream
   [HASH]..[HASH]  master     -> origin/master
Updating [HASH]..[HASH]
Fast-forward
 test  | 0
 test2 | 0
 2 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 test
 create mode 100644 test2
(HEAD -> master, origin/master) Merge remote-tracking branch 'remotes/cvsupstream/master' into sync/cvs_test/master
Commit 2
Merge remote-tracking branch 'remotes/cvsupstream/master' into sync/cvs_test/master
Initial commit
Commit in downstream
EOL
}

function get_stderr()
{
    cat<<EOL
Cloning into 'downstreamwc.git'...
warning: You appear to have cloned an empty repository.
done.
To /tmp/any2gittestroot/downstream.git
 * [new branch]      master -> master
cvs add: scheduling file \`test2' for addition
cvs add: use \`cvs commit' to add this file permanently
cvs commit: Examining .
EOL
}
