CREATE_TEST_CVS=1

function run_test()
{
    cat<<EOL > config
profiles:
    cvs_test:
        repo:
            type: cvs
            upstream: $TEST_CVS
            upstream_module: $TEST_CVS_MODULE
            mirror: $ROOT/mirror.gitcvs
            downstream: $ROOT/downstream.git

        branch_mapping:
            master: master

        merging:
            strategy: merge
EOL

    $ANY2GIT -c config init > /dev/null > /dev/null

    git clone downstream.git wc.git
    cd wc.git
    git log --topo-order --oneline | cut -c 9-
    cd ..

    cd $TEST_CVS_WC
    touch test2
    cvs add test2
    cvs commit -m "Commit 2"
    cd ..

    $ANY2GIT -c config sync > /dev/null > /dev/null

    cd $ROOT/wc.git
    git pull |& hash_filter
    git log --topo-order --oneline master |cut -c 9-
    cd ..

    cat<<EOL > config
profiles:
    cvs_test:
        repo:
            type: cvs
            upstream: $TEST_CVS
            upstream_module: $TEST_CVS_MODULE
            mirror: $ROOT/mirror.gitcvs
            downstream: $ROOT/downstream.git

        branch_mapping:
            master: [master, master2]

        merging:
            strategy: merge
EOL

    $ANY2GIT -c config sync > /dev/null > /dev/null

    cd $ROOT/wc.git
    git pull |& hash_filter
    git log --topo-order --oneline master |cut -c 9-
    git log --topo-order --oneline origin/master2 |cut -c 9-
    cd ..
}

function get_stdout()
{
    cat<<EOL
(HEAD -> master, origin/master, origin/HEAD) Initial commit
/tmp/any2gittestroot/test.cvs/test_module/test2,v  <--  test2
initial revision: 1.1
From /tmp/any2gittestroot/downstream
   [HASH]..[HASH]  master     -> origin/master
Updating [HASH]..[HASH]
Fast-forward
 test2 | 0
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 test2
(HEAD -> master, origin/master, origin/HEAD) Commit 2
Initial commit
From /tmp/any2gittestroot/downstream
 * [new branch]      master2    -> origin/master2
Already up to date.
(HEAD -> master, origin/master2, origin/master, origin/HEAD) Commit 2
Initial commit
(HEAD -> master, origin/master2, origin/master, origin/HEAD) Commit 2
Initial commit
EOL
}

function get_stderr()
{
    cat<<EOL
Cloning into 'wc.git'...
done.
cvs add: scheduling file \`test2' for addition
cvs add: use \`cvs commit' to add this file permanently
cvs commit: Examining .
EOL
}
