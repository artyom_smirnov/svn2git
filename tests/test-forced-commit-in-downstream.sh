CREATE_TEST_SVN=1

function run_test()
{
    cat<<EOL > config
profiles:
    svn_test:
        repo:
            type: svn
            upstream: $TEST_SVN
            mirror: $ROOT/mirror.gitsvn
            downstream: $ROOT/downstream.git

        branch_mapping:
            trunk: master

        merging:
            strategy: merge
EOL
    mkdir downstream.git
    cd downstream.git
    git init --bare
    cd ..

    git clone downstream.git downstreamwc.git
    cd downstreamwc.git
    touch myfile
    git add myfile
    git commit -m 'Initial GIT commit' | hash_filter
    touch myfile2
    git add myfile2
    git commit -m 'Commit in downstream 1' | hash_filter
    git push
    cd ..

    $ANY2GIT -c config init --existing-downstream | date_filter > /dev/null
    $ANY2GIT -c config sync | date_filter > /dev/null

    cd $TEST_SVN_WC/trunk
    echo test > test
    svn add test
    svn commit -m "SVN commit 1"
    cd ../..

    $ANY2GIT -c config sync | date_filter | hash_filter > /dev/null

    cd $ROOT/downstreamwc.git
    git pull |& hash_filter
    git log --topo-order --oneline |cut -c 9-
    git reset HEAD^^^ --hard | hash_filter
    echo qwe >> myfile
    git add myfile
    git commit -m 'Commit in downstream 2' | hash_filter
    git push --force |& hash_filter
    cd ..

    cd $TEST_SVN_WC/trunk
    echo test2 > test
    svn commit -m "SVN commit 2"
    cd ../..

    $ANY2GIT -c config sync | date_filter | hash_filter > /dev/null

    cd $ROOT/downstreamwc.git
    git pull |& hash_filter
    git log --topo-order --oneline |cut -c 9-
    cd ..
}

function get_stdout()
{
    cat<<EOL
Initialized empty Git repository in /tmp/any2gittestroot/downstream.git/
[master (root-commit) HASH] Initial GIT commit
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 myfile
[master HASH] Commit in downstream 1
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 myfile2
A         test
Adding         test
Transmitting file data .done
Committing transaction...
Committed revision 2.
From /tmp/any2gittestroot/downstream
   [HASH]..[HASH]  master     -> origin/master
Updating [HASH]..[HASH]
Fast-forward
 test | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 test
(HEAD -> master, origin/master) Merge remote-tracking branch 'remotes/svnupstream/trunk' into sync/svn_test/master
SVN commit 1
Merge remote-tracking branch 'remotes/svnupstream/trunk' into sync/svn_test/master
Initial
Commit in downstream 1
Initial GIT commit
HEAD is now at [HASH] Initial GIT commit
[master HASH] Commit in downstream 2
 1 file changed, 1 insertion(+)
To /tmp/any2gittestroot/downstream.git
 + [HASH]..[HASH] master -> master (forced update)
Sending        test
Transmitting file data .done
Committing transaction...
Committed revision 3.
From /tmp/any2gittestroot/downstream
   [HASH]..[HASH]  master     -> origin/master
Updating [HASH]..[HASH]
Fast-forward
 test | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 test
(HEAD -> master, origin/master) Merge remote-tracking branch 'remotes/svnupstream/trunk' into sync/svn_test/master
SVN commit 2
SVN commit 1
Initial
Commit in downstream 2
Initial GIT commit
EOL
}

function get_stderr()
{
    cat<<EOL
Cloning into 'downstreamwc.git'...
warning: You appear to have cloned an empty repository.
done.
To /tmp/any2gittestroot/downstream.git
 * [new branch]      master -> master
EOL
}
