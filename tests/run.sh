#!/bin/bash
TESTSDIR=$(dirname $(readlink -f $0))
ANY2GITDIR=$(dirname ${TESTSDIR})
PYTHON=${PYTHON:=python}
ANY2GIT="${PYTHON} ${ANY2GITDIR}/any2git.py"
RESULT=0

function die()
{
    echo "Test suite failure: $1"
    exit 1
}

function prepare_svn()
{
    mkdir test.svn && cd test.svn && svnadmin create . || die "Unable create SVN repo"
    TEST_SVN=file://`pwd`
    cd ..

    svn co $TEST_SVN wc.tmp > /dev/null 2>&1 || die "Unable checkout from SVN"

    cd  wc.tmp
    TEST_SVN_WC=`pwd`
    mkdir trunk
    svn add trunk > /dev/null 2>&1 || die "Unable add to SVN"
    svn commit -m "Initial" > /dev/null 2>&1|| die "Unable commit to SVN"
    cd ..
}

function prepare_cvs()
{
    TEST_CVS=`pwd`/test.cvs
    TEST_CVS_MODULE=test_module
    TEST_CVS_WC=`pwd`/wc.cvs

    cvs -d $TEST_CVS init || die "Unable to create CVS repo"

    mkdir $TEST_CVS_MODULE
    cd $TEST_CVS_MODULE
    cvs -d $TEST_CVS import -m "Initial import" $TEST_CVS_MODULE vtag rtag > /dev/null 2>&1 || die "Unable import to CVS"
    cd ..

    cvs -d $TEST_CVS checkout -d wc.cvs $TEST_CVS_MODULE > /dev/null 2>&1 || die "Unable checkout from CVS"

    cd wc.cvs
    touch test
    cvs add test > /dev/null 2>&1 || die "Unable add to CVS"
    cvs commit -m "Initial commit" > /dev/null 2>&1 || die "Unable commit to CVS"
    cd ..
}

function prepare_git()
{
    TEST_GIT=`pwd`/test.git
    TEST_GIT_WC=`pwd`/wc.git

    mkdir test.git
    cd test.git
    git init --bare > /dev/null 2>&1 || die "Unable to create GIT repo"
    cd ..

    git clone test.git wc.git > /dev/null 2>&1 || die "Unable to checkout from GIT repo"

    cd wc.git
    touch test
    git add test || die "Unable add to GIT"
    git commit -m "Initial commit"  > /dev/null 2>&1 || die "Unable commit to GIT"
    git push > /dev/null 2>&1 || die "Unable push to GIT"
    cd ..
}

function date_filter()
{
    sed 's/[0-9]*:[0-9]*:[0-9]* [0-9]*.[0-9]*.[0-9]*/[TIMESTAMP]/g'
}

function hash_filter()
{
    sed -e 's/[0-9a-f]\{40\}/\[HASH\]/g' -e 's/[0-9a-f]\{7\}\.\.\.\?[0-9a-f]\{7\}/[HASH]..[HASH]/g' -e 's/\(\[[^ ]\+\( (.*)\)\? \)[0-9a-f]\{7\}\+\]/\1HASH]/' \
        -e 's/is now at [0-9a-f]\{7\}/is now at [HASH]/'
}


if [ "`uname -s`" == "Darwin" ]; then
    MD5SUM=md5
else
    MD5SUM=md5sum
fi

if [ $# -eq 0 ]; then
    tests=`ls ${TESTSDIR}/test-*.sh`
else
    tests=`readlink -f $@`
fi
[ $? != 0 ] && exit 1

for test in $tests; do
    echo -n "`basename ${test}` ... "
    export ROOT=/tmp/any2gittestroot
    rm -rf "$ROOT"
    mkdir -p "$ROOT"
    cd "$ROOT"

    CREATE_TEST_SVN=0
    unset TEST_SVN
    unset TEST_SVN_WC
    CREATE_TEST_CVS=0
    unset TEST_CVS
    unset TEST_CVS_MODULE
    unset TEST_CVS_WC
    CREATE_TEST_GIT=0
    unset TEST_GIT
    unset TEST_GIT_WC

    . $test

    if [ "$CREATE_TEST_SVN" == "1" ]; then
        prepare_svn
    fi

    if [ "$CREATE_TEST_CVS" == "1" ]; then
        prepare_cvs
    fi

    if [ "$CREATE_TEST_GIT" == "1" ]; then
        prepare_git
    fi

    run_test 1>/tmp/test.out.actual 2>/tmp/test.err.actual
    get_stdout > /tmp/test.out.expected
    get_stderr > /tmp/test.err.expected

    stdout_differ=0
    if [ "`cat /tmp/test.out.actual | $MD5SUM`" != "`cat /tmp/test.out.expected | $MD5SUM`" ]; then
        stdout_differ=1
    fi

    stderr_differ=0
    if [ "`cat /tmp/test.err.actual | $MD5SUM`" != "`cat /tmp/test.err.expected | $MD5SUM`" ]; then
        stderr_differ=1
    fi
    
    if [ $stdout_differ != 0 -o $stderr_differ != 0 ]; then
        echo FAIL
        RESULT=1
    else
        echo OK
    fi

    if [ $stdout_differ != 0  ]; then
        echo stdout diff:
        diff --unified /tmp/test.out.{expected,actual}
        if [ "$TESTDEBUG" == "1" ]; then
            echo stdout:
            cat /tmp/test.out.actual
        fi
    fi

    if [ $stderr_differ != 0  ]; then
        echo stderr diff:
        diff --unified /tmp/test.err.{expected,actual}
        if [ "$TESTDEBUG" == "1" ]; then
            echo stderr:
            cat /tmp/test.err.actual
        fi
    fi

    [ "$TESTDEBUG" == "1" ] && exit 0

    unset -f run_test get_stdout get_stderr 2&>1 > /dev/null
    rm -rf "$ROOT" /tmp/test.out.{expected,actual} /tmp/test.err.{expected,actual}
done

exit $RESULT
