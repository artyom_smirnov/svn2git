CREATE_TEST_SVN=1

function run_test()
{
    cat<<EOL > config
profiles:
    svn_test:
        repo:
            type: svn
            upstream: $TEST_SVN
            mirror: $ROOT/mirror.gitsvn
            downstream: $ROOT/downstream.git

        branch_mapping:
            trunk: master

        merging:
            strategy: merge
EOL
    $ANY2GIT -c config init | date_filter > /dev/null
    cd $ROOT/downstream.git
    git log --topo-order --oneline |cut -c 9-
    cd ..

    git clone downstream.git wc.git
    cd wc.git
    git log --topo-order --oneline |cut -c 9-
    cd ..

    cd $TEST_SVN_WC/trunk
    echo test > test
    svn add test
    svn commit -m "test commit"
    cd ../..

    $ANY2GIT -c config sync | date_filter | hash_filter > /dev/null

    cd $ROOT/wc.git
    git pull |& hash_filter
    git log --topo-order --oneline |cut -c 9-
    cd ..
}

function get_stdout()
{
    cat<<EOL
(HEAD -> master) Initial
(HEAD -> master, origin/master, origin/HEAD) Initial
A         test
Adding         test
Transmitting file data .done
Committing transaction...
Committed revision 2.
From /tmp/any2gittestroot/downstream
   [HASH]..[HASH]  master     -> origin/master
Updating [HASH]..[HASH]
Fast-forward
 test | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 test
(HEAD -> master, origin/master, origin/HEAD) test commit
Initial
EOL
}

function get_stderr()
{
    cat<<EOL
Cloning into 'wc.git'...
done.
EOL
}
