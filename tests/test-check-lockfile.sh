CREATE_TEST_SVN=1

function run_test()
{
    cat<<EOL > config
profiles:
    svn_test:
        repo:
            type: svn
            upstream: $TEST_SVN
            mirror: $ROOT/mirror.gitsvn
            downstream: $ROOT/downstream.git

        branch_mapping:
            trunk: master

        merging:
            strategy: merge

        lockfile: $ROOT/lockfile
EOL
    touch $ROOT/lockfile
    $ANY2GIT -c config init | date_filter > /dev/null

    rm $ROOT/lockfile
    $ANY2GIT -c config init | date_filter > /dev/null
}

function get_stdout()
{
    cat<<EOL
EOL
}

function get_stderr()
{
:
}
