#!/usr/bin/python
import re
import yaml
import argparse
import subprocess
import os
import sys
import time
import smtplib
import jinja2
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import traceback
from configuration import Configuration

template_plain = """\
{% if rejected %}
Rejected commits\n
From upstream branch | To downstream branch | Commit hash | Message\n
{%for item in rejected%}
{{item[0]}} {{item[1]}} {{item[2]}} {{item[3]}}\n
{%endfor%}
{% endif %}

{% if accepted %}
Accepted commits\n
From upstream | To downstream | Message\n
{%for item in accepted%}
{{item[0]}} ({{item[2]}}) | {{item[1]}} ({{item[4]}}) | {{item[3]}}\n
{%endfor%}
{% endif %}

Log of operations:
{{log}}
"""

template_html = """\
<html>
<head>
<style TYPE="text/css">
.rejected
{
    font-size: 1.5em;
    color: red;
}

.accepted
{
    font-size: 1.5em;
    color: green;
}

.log
{
    font-size: 1.5em;
}

table.results
{
    border-collapse: collapse;
}

table.results td,th
{
    border-width: 1px;
    border-style: solid;
}
</style>
</head>
<body>

{% if rejected %}
<h1 class='rejected'>Rejected commits</h1>
<table class='results'>
<tr><th>From upstream</th><th>To downstream</th><th>Message</th></tr>
{%for item in rejected%}
<tr><td>{{item[0]}} ({{item[2]}})</td><td>{{item[1]}}</td><td>{{item[3]}}</td></tr>
{%endfor%}
</table>
{% endif %}

{% if accepted %}
<h1 class='accepted'>Accepted commits</h1>
<table class='results'>
<tr><th>From upstream</th><th>To downstream</th><th>Message</th></tr>
{%for item in accepted%}
<tr><td>{{item[0]}} ({{item[2]}})</td><td>{{item[1]}} ({{item[4]}})</td><td>{{item[3]}}</td></tr>
{%endfor%}
</table>
{% endif %}

<h1 class='log'>Log of operations</h1>
<pre>
{{log}}
</pre>
</body>
</html>"""

ACTION_INIT = 'init'
ACTION_SYNC = 'sync'
SVN = 'svn'
CVS = 'cvs'
GIT = 'git'

CHERRY_PICK = 'cherry-pick'
MERGE = 'merge'


def log(msg):
    msg = '%s %s\n' % (time.strftime("%H:%M:%S %Y.%m.%d", time.localtime()), msg)
    Configuration.log_string.write(msg)
    Configuration.log_file.write(msg)


def log_exit(msg, result=1, accepted=None, rejected=None, send_email=True):
    log(msg)

    if send_email and Configuration.email_from and (Configuration.email_to_always or Configuration.email_to_onfail):
        email_to = Configuration.email_to_always
        if result:
            email_to += Configuration.email_to_onfail

        log("Sending email")
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Merging status of project %s: %s" % (
            Configuration.profile_name, 'FAIL' if result else 'SUCCESS')
        msg['From'] = Configuration.email_from
        msg['To'] = ', '.join(email_to)

        kwargs_plain = {
            'accepted': accepted,
            'rejected': rejected,
            'log': Configuration.log_string.getvalue()
        }

        kwargs_html = {
            'accepted': accepted,
            'rejected': rejected,
            'log': Configuration.log_string.getvalue(),
            'mirror': Configuration.mirror_dir,
            'profile': Configuration.profile_name
        }
        if accepted:
            for c in kwargs_html['accepted']:
                c[3] = c[3].replace('\n', '<br>')
        if rejected:
            for c in kwargs_html['rejected']:
                c[3] = c[3].replace('\n', '<br>')

        plain = jinja2.Template(template_plain)
        html = jinja2.Template(template_html)
        plain = plain.render(**kwargs_plain)
        html = html.render(**kwargs_html)
        msg.attach(MIMEText(plain, 'plain'))
        msg.attach(MIMEText(html, 'html'))

        s = smtplib.SMTP(Configuration.email_server)
        s.sendmail(Configuration.email_from, email_to, msg.as_string())
        s.quit()

    Configuration.log_file.close()

    exit(result)


def run_and_wait(arguments):
    log('> ' + ' '.join(arguments))
    proc = subprocess.Popen(arguments,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            universal_newlines=True)
    (out, err) = proc.communicate()

    return proc.returncode, out, err


def load_config(config):
    f = open(config)
    m = yaml.safe_load(f)
    f.close()
    return m


def main():
    send_email = True
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--config', type=str, required=True, help='Configuration file')

        subparsers = parser.add_subparsers(dest='action')
        parser_init = subparsers.add_parser(ACTION_INIT)
        parser_init.add_argument('profile', nargs='?', help='Profile to init')
        parser_init.add_argument('--existing-mirror', action='store_true',
                                 help='Do not clone upstream to mirror and use existing')
        parser_init.add_argument('--existing-downstream', action='store_true',
                                 help='Do not create downstream and use existing')

        parser_sync = subparsers.add_parser(ACTION_SYNC)
        parser_sync.add_argument('profile', nargs='?', help='Profile to sync')
        parser_sync.add_argument('--no-email', action='store_true', help='Do not send email on fail')
        parser_sync.add_argument('--log', type=str, help='Path to log')

        args = parser.parse_args()
        m = load_config(args.config)

        if args.action == ACTION_INIT:
            send_email = False
        elif args.action == ACTION_SYNC:
            send_email = True
        else:
            log_exit('Unknown action: %s' % args.action)

        if 'log' in args and args.log:
            try:
                f = open(args.log, "a")
                Configuration.log_file = f
            except Exception as e:
                log_exit("Unable to open log file %s: %s" % (args.log, e), send_email=send_email)

        if 'mail' in m:
            mail = m['mail']
            if 'server' in mail:
                Configuration.email_server = mail['server']
            if 'from' not in mail:
                log_exit('No from section in mail section', send_email=send_email)
            Configuration.email_from = mail['from']
            if 'to_always' in mail:
                Configuration.email_to_always = mail['to_always']
            if 'to_onfail' in mail:
                Configuration.email_to_onfail = mail['to_onfail']

        if 'profiles' not in m:
            log_exit('No profiles section in config', send_email=send_email)

        profiles = m['profiles']

        if len(profiles) < 1:
            log_exit('Profiles section is empty', send_email=send_email)
        if len(profiles) > 1 and not args.profile:
            log_exit('Choose one of profiles: %s' % ','.join(profiles.keys()), send_email=send_email)
        if args.profile and args.profile not in profiles:
            log_exit('Profile %s not exists' % args.profile, send_email=send_email)

        if len(profiles) == 1:
            profile = list(profiles.values())[0]
            Configuration.profile_name = list(profiles.keys())[0]
            log('Using profile %s' % Configuration.profile_name)
        else:
            profile = profiles[args.profile]
            Configuration.profile_name = args.profile
            log('Using profile %s' % args.profile)

        if args.action == ACTION_INIT:
            init(Configuration.profile_name, profile, not args.existing_mirror, not args.existing_downstream)
        elif args.action == ACTION_SYNC:
            sync(Configuration.profile_name, profile)

        Configuration.log_file.close()
    except Exception as e:
        stack = format_exception()
        log("Caught unhandled exception: %s" % e)
        log_exit(stack, send_email=send_email)


class Profile:
    def __init__(self):
        self.type = None
        self.upstream = None
        self.mirror = None
        self.downstream = None
        self.mirror_base = None
        self.mirror_dir = None
        self.downstream_base = None
        self.downstream_dir = None
        self.branch_mapping = None
        self.downstream_name = None
        self.upstream_module = None
        self.merge_strategy = CHERRY_PICK
        self.authors_file = None
        self.authors_prog = None
        self.lockfile = None
        self.ff_branches = []


def load_profile(name, profile, init=False):
    p = Profile()
    p.downstream_name = name
    log("Loading profile")
    if 'repo' not in profile:
        log_exit("No repo section in profile", send_email=not init)
    if 'branch_mapping' not in profile:
        log_exit("No branch_mapping section in profile", send_email=not init)

    repo = profile['repo']
    if 'type' not in repo:
        log_exit("Unknown upstream repository type", send_email=not init)
    p.type = repo['type'].lower()

    if p.type == SVN:
        from svn import init_mirror, fetch_commits, upstream_prefix
    elif p.type == CVS:
        from cvs import init_mirror, fetch_commits, upstream_prefix
        if 'upstream_module' not in repo:
            log_exit("No upstream module in repo section", send_email=not init)
        p.upstream_module = repo['upstream_module']
    elif p.type == GIT:
        from git import init_mirror, fetch_commits, upstream_prefix
    else:
        log_exit("Upstream repo type '%s' not supported" % p.type, send_email=not init)
    Configuration.init_mirror = staticmethod(init_mirror)
    Configuration.fetch_commits = staticmethod(fetch_commits)
    Configuration.upstream_prefix = staticmethod(upstream_prefix)

    p.branch_mapping = profile['branch_mapping']

    # We expect to have list of values if no create it manually
    for source, destinations in p.branch_mapping.items():
        if type(destinations) not in (list, tuple):
            p.branch_mapping[source] = [destinations]

    if 'upstream' not in repo or 'mirror' not in repo or 'downstream' not in repo:
        log_exit("Section repo should have upstream, mirror and downstream items", send_email=not init)

    if 'merging' in profile:
        strategy = profile['merging']['strategy']
        if strategy not in [CHERRY_PICK, MERGE]:
            log_exit("Merging strategy can be 'cherry-pick' or 'merge'", send_email=not init)
        p.merge_strategy = strategy

    if 'authors' in profile:
        if 'file' in profile['authors']:
            p.authors_file = profile['authors']['file']
        if 'prog' in profile['authors']:
            p.authors_prog = profile['authors']['prog']

    if 'lockfile' in profile:
        p.lockfile = profile['lockfile']

    p.upstream = repo['upstream']
    p.mirror = Configuration.mirror_dir = repo['mirror']
    p.downstream = repo['downstream']
    (p.mirror_base, p.mirror_dir) = os.path.split(p.mirror)
    (p.downstream_base, p.downstream_dir) = os.path.split(p.downstream)

    if 'ff_branches' in profile:
        p.ff_branches = profile['ff_branches']
        if type(p.ff_branches) not in (list, tuple):
            p.ff_branches = [p.ff_branches]

    return p


def setup_branches(profile, init=False):
    p = profile
    os.chdir(p.mirror)
    log("Fetching downstream to mirror")
    (rc, out, err) = run_and_wait(['git', 'fetch', p.downstream_name])
    if rc:
        log_exit(err, send_email=not init)
    log("Collecting information about branches")
    (rc, out, err) = run_and_wait(['git', 'branch', '-a'])
    if rc:
        log_exit(err, send_email=not init)

    r = re.compile(r'^\s*remotes/(%s/[^\s]*)$' % Configuration.upstream_prefix())
    upstream_branches = []
    for line in out.split('\n'):
        res = r.match(line)
        if res:
            upstream_branches.append(res.groups()[0])

    r = re.compile(r'\*?\s*sync/%s/(.*)' % p.downstream_name)
    sync_branches = []
    for line in out.split('\n'):
        res = r.match(line)
        if res:
            sync_branches.append(res.groups()[0])

    r = re.compile(r'\*?\s*remotes/%s/(.*)' % p.downstream_name)
    downstream_branches = []
    for line in out.split('\n'):
        res = r.match(line)
        if res:
            downstream_branches.append(res.groups()[0])

    for source, destinations in p.branch_mapping.items():
        source = '%s/%s' % (Configuration.upstream_prefix(), source)
        for dest in destinations:
            if source not in upstream_branches:
                log_exit('Can not find upstream branch %s' % source, send_email=not init)

            if dest not in sync_branches:
                if dest not in downstream_branches:
                    log('Setup new sync branch: from %s to %s' % (source, dest))
                    (rc, out, err) = run_and_wait(
                        ['git', 'checkout',
                         'remotes/%s' % source,
                         '-b',
                         'sync/%s/%s' % (p.downstream_name, dest)])
                    if rc:
                        log_exit(err, send_email=not init)
                    (rc, out, err) = run_and_wait(
                        ['git', 'push', p.downstream_name, 'sync/%s/%s:%s' % (p.downstream_name, dest, dest)])
                    if rc:
                        log_exit(err, send_email=not init)
                else:
                    log('Setup existing downstream branch for syncing: from %s to %s' % (source, dest))
                    (rc, out, err) = run_and_wait(
                        ['git', 'checkout',
                         'remotes/%s/%s' % (p.downstream_name, dest),
                         '-b',
                         'sync/%s/%s' % (p.downstream_name, dest)])
                    if rc:
                        log_exit(err, send_email=not init)

    return upstream_branches, sync_branches, downstream_branches


def lockfile(profile):
    if not profile.lockfile:
        return

    if os.path.exists(profile.lockfile):
        log_exit('Profile locked with lockfile %s' % profile.lockfile)

    try:
        log('Locking profile with lockfile %s' % profile.lockfile)
        with open(profile.lockfile, 'w') as f:
            f.close()
    except Exception as e:
        log_exit('Unable to create lockfile %s: %s' % (profile.lockfile, e))

    import atexit
    atexit.register(os.unlink, profile.lockfile)


def init(profile_name, profile, create_mirror, create_downstream):
    p = load_profile(profile_name, profile, init=True)
    lockfile(p)

    if not os.path.exists(p.mirror_base):
        log_exit('Directory %s not exists' % p.mirror_base, send_email=False)

    if os.path.exists(p.mirror) and create_mirror:
        log_exit('Mirror destination %s already exists' % p.mirror, send_email=False)

    if not os.path.exists(p.downstream_base) and create_downstream:
        log_exit('Directory %s not exists' % p.downstream_base, send_email=False)

    if os.path.exists(p.downstream) and create_downstream:
        log_exit('Downstream destination %s already exists' % p.downstream, send_email=False)

    if create_mirror:
        Configuration.init_mirror(p)

    if create_downstream:
        os.chdir(p.downstream_base)
        log("Create downstream from %s to %s" % (p.mirror, p.downstream))
        (rc, out, err) = run_and_wait(['git', 'init', '--bare', p.downstream_dir])
        if rc:
            log_exit(err, send_email=False)

    os.chdir(p.mirror)
    log("Setup downstream remote %s as %s" % (p.downstream, p.downstream_name))
    (rc, out, err) = run_and_wait(['git', 'remote', 'add', p.downstream_name, p.downstream])
    if rc:
        log_exit(err, send_email=False)

    setup_branches(p, init=True)


def sync(profile_name, profile):
    p = load_profile(profile_name, profile)
    lockfile(p)

    if not os.path.exists(p.mirror):
        log_exit('Directory %s not exists' % p.mirror)

    os.chdir(p.mirror)
    log('Cleanup mirror before actions')
    run_and_wait(['git', 'reset', '--hard'])
    run_and_wait(['git', 'clean', '-fdx'])

    setup_branches(p)

    commits = []
    Configuration.fetch_commits(p, commits)

    if commits:
        log("New commits in upstream")
        for branch, hash, _ in commits:
            log("%s %s" % (hash, branch))

    log("Fetching downstream changes")
    (rc, out, err) = run_and_wait(
        ['git', 'fetch', p.downstream_name])
    if rc:
        log_exit(err)

    applied_commits = []
    rejected_commits = []
    # Branch syncing loop
    # Note we do not care about state of syncing branch, we just take downstream branch, applying new commits and
    # pushing it back. We assume that this is our initial branch but we do not care much because downstream history
    # can be changed by 'git push --force' and we fail on pull so we pull with --force too.
    for source, destinations in p.branch_mapping.items():
        source = '%s/%s' % (Configuration.upstream_prefix(), source)
        for dest in destinations:
            # Switching to proper branch first
            log("Preparing sync branch for %s" % dest)
            run_and_wait(['git', 'reset', '--hard'])
            run_and_wait(['git', 'clean', '-fdx'])
            (rc, out, err) = run_and_wait(['git', 'checkout', 'sync/%s/%s' % (p.downstream_name, dest)])
            if rc:
                log_exit(err)

            # Resetting current sync branch
            (rc, out, err) = run_and_wait(
                ['git', 'reset', '--hard', '%s/%s' % (p.downstream_name, dest)])
            if rc:
                log_exit(err)

            if p.merge_strategy == CHERRY_PICK:
                # Cherry-picking each commit from appropriate upstream svn branch
                for commit_source, commit_hash, msg in commits:
                    if source == commit_source:
                        log("Applying new commit from upstream %s to downstream %s" % (source, dest))
                        run_and_wait(['git', 'reset', '--hard'])
                        run_and_wait(['git', 'clean', '-fdx'])
                        (rc, out, err) = run_and_wait(['git', 'cherry-pick', commit_hash])
                        # If we fail on cherry-picking we do not halt process we will send
                        # email that some commits should be applied manually
                        if rc:
                            log(out)
                            log(err)
                            rejected_commits.append([source, dest, commit_hash, msg])
                            run_and_wait(['git', 'reset', '--hard'])
                            run_and_wait(['git', 'clean', '-fdx'])
                        else:
                            (rc, out, err) = run_and_wait(['git', 'rev-parse', 'HEAD'])
                            head_rev = out.strip()
                            applied_commits.append([source, dest, commit_hash, msg, head_rev])
            elif p.merge_strategy == MERGE:
                log("Merging upstream branch %s to downstream branch %s" % (source, dest))
                args = ['git', 'merge', '--allow-unrelated-histories', '--log']
                if dest in p.ff_branches:
                    args.append('--ff-only')
                args.append('remotes/%s' % source)
                (rc, out, err) = run_and_wait(args)
                if rc:
                    log(out)
                    log(err)
                    run_and_wait(['git', 'reset', '--hard'])
                    run_and_wait(['git', 'clean', '-fdx'])
                    for commit_source, commit_hash, msg in commits:
                        if source == commit_source:
                            rejected_commits.append([source, dest, commit_hash, msg])
                else:
                    for commit_source, commit_hash, msg in commits:
                        if source == commit_source:
                            applied_commits.append([source, dest, commit_hash, msg, ''])
            else:
                log("Do not know merging strategy %s" % p.merge_strategy)

            # Now push back changes to downstream
            log("Pushing changes to downstream")
            (rc, out, err) = run_and_wait(
                ['git', 'push', p.downstream_name, 'sync/%s/%s:%s' % (p.downstream_name, dest, dest)])
            if rc:
                log_exit(err, accepted=applied_commits, rejected=rejected_commits)

    if len(applied_commits) or len(rejected_commits):
        log("Summary")
        if len(rejected_commits):
            log("Rejected commits")
            for source, dest, hash, _ in rejected_commits:
                log("%s (%s) ->x  %s" % (source, hash, dest))
        if len(applied_commits):
            log("Applied commits")
            for source, dest, hash_source, _, hash_dest in applied_commits:
                log("%s (%s) -> %s (%s)" % (source, hash_source, dest, hash_dest))

    if applied_commits or rejected_commits:
        log_exit('Done', result=(1 if rejected_commits else 0), accepted=applied_commits, rejected=rejected_commits)


def format_exception():
    exception_list = traceback.format_stack()
    exception_list = exception_list[:-2]
    exception_list.extend(traceback.format_tb(sys.exc_info()[2]))
    exception_list.extend(traceback.format_exception_only(sys.exc_info()[0], sys.exc_info()[1]))

    exception_str = "Traceback (most recent call last):\n"
    exception_str += "".join(exception_list)
    # Removing the last \n
    exception_str = exception_str[:-1]

    return exception_str


if __name__ == '__main__':
    main()
